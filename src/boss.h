#ifndef _BOSS_H_
#define _BOSS_H_

RcHitEnum MoveBossBomb(CharacterData *my);
RcHitEnum HitBossToBomb(CharacterData *my, CharacterData *your);
void CreateBoss1(void);
RcHitEnum MoveBoss1(CharacterData *my);
RcHitEnum MoveBoss2(CharacterData *my);

#endif
