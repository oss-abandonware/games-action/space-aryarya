#include "geki4.h"
#include "extern.h"

/********************************
  イメージ描画(フレームあり)
********************************/
void DrawPixmapFrame3D(CharacterData *my)
{
  KXL_PutImage(PixKage[0]->Image,
	       ADD_X + my->X -48,
	       ADD_Y + 130); 
  KXL_PutImage(my->Spr[my->FrameNo]->Image,
	       ADD_X + my->X - my->Spr[0]->Image->Width  / 2,
	       ADD_Y + my->Y - my->Spr[0]->Image->Height / 2); 
}

/********************************
  イメージ描画(影付き)
********************************/
void DrawPixmap3D(CharacterData *my)
{
  Sint16 z = BASE_Z + my->Z;
  KXL_PutImage(PixKage[my->Z]->Image,
	       ADD_X + my->X * BASE_Z / z + PixKage[my->Z]->AddX,
	       ADD_Y + 150 * BASE_Z / z); 
  KXL_PutImage(my->Spr[my->Z]->Image,
	       ADD_X + my->X * BASE_Z / z + my->Spr[my->Z]->AddX,
	       ADD_Y + my->Y * BASE_Z / z + my->Spr[my->Z]->AddY); 
}

/********************************
  イメージ描画(影なし)
********************************/
void DrawPixmap3DNone(CharacterData *my)
{
  Sint16 z = BASE_Z + my->Z;
  KXL_PutImage(my->Spr[my->Z]->Image,
	       ADD_X + my->X * BASE_Z / z + my->Spr[my->Z]->AddX,
	       ADD_Y + my->Y * BASE_Z / z + my->Spr[my->Z]->AddY); 
}

/**********************
  当たったら砕けろ
 **********************/
RcHitEnum HitDelete(CharacterData *my, CharacterData *your)
{
  return RcHitDel;
}

/**********************
  当たってもへっちゃら
 **********************/
RcHitEnum HitNone(CharacterData *my, CharacterData *your)
{
  KXL_PlaySound(SE_HIT, KXL_SOUND_PLAY);
  return RcHitNone;
}

/**********************
  自分からは動かない
 **********************/
RcHitEnum MoveNone(CharacterData *my)
{
  return RcHitNone;
}

/****************************
  跳ね返りチェック
 ****************************/
void CheckBound(CharacterData *my)
{
  if (my->X + my->AddX < DRAW_LEFT + my->Spr[my->Z]->Image->Width / 2 ||
      my->X + my->AddX > DRAW_RIGHT - my->Spr[my->Z]->Image->Width / 2)
    my->AddX = -(my->AddX);
  if (my->Y + my->AddY < DRAW_TOP + my->Spr[my->Z]->Image->Height / 2 ||
      my->Y + my->AddY > DRAW_BOTTOM - my->Spr[my->Z]->Image->Height / 2)
    my->AddY = -(my->AddY);
}

/****************************
  停止チェック
 ****************************/
void CheckStop(CharacterData *my)
{
  if (my->X < DRAW_LEFT + my->Spr[my->Z]->Image->Width / 2)
    my->X = DRAW_LEFT + my->Spr[my->Z]->Image->Width / 2;
  if (my->X > DRAW_RIGHT - my->Spr[my->Z]->Image->Width / 2)
    my->X = DRAW_RIGHT - my->Spr[my->Z]->Image->Width / 2;
  if (my->Y < DRAW_TOP + my->Spr[my->Z]->Image->Height / 2)
    my->Y = DRAW_TOP + my->Spr[my->Z]->Image->Height / 2;
  if (my->Y > DRAW_BOTTOM - my->Spr[my->Z]->Image->Height / 2)
    my->Y = DRAW_BOTTOM - my->Spr[my->Z]->Image->Height / 2;
}

/********************************
  ステージクリア&ゲームオーバー
********************************/
void ClearAndGameOver(void)
{
  static Uint16 no;
  Uint16 i;
  Uint8 *str[3] = {"Clear", "GameOver", "See you next..."};

  switch (Root->Cnt) {
  case 0:
    KXL_PlaySound(0, KXL_SOUND_STOP_ALL);
    ScoreRanking();
    no = Root->MainFlag == MainGameOver ? 1 : Root->Stage == STAGE_MAX - 1 ? 2 : 0;
    break;
  case 100:
    DeleteStage();
    Root->Cnt = -1;
    if (Root->MainFlag == MainClear) {
      if (Root->Stage == STAGE_MAX - 1) {
	Root->MainFlag = MainOpening;
      } else {
	Root->Stage ++;
	for (i = 0; i < MAX_YOUR; i ++)
	  Root->Your[i]->Chr.Active = False;
	for (i = 0; i < MAX_MY; i ++)
	  Root->My[i]->Chr.Active = False;
	CreateMy();
	Root->MainFlag = MainGame;
	LoadStage();
      }
    } else {
      Root->MainFlag = MainOpening;
    }
    break;
  default:
    KXL_Font("-bitstream-charter-bold-*-normal-*-60-*-*-*-*-*-*-*", 0x00, 0x00, 0x00);
    KXL_PutText(AREA_LX + 2 + (DRAW_WIDTH - KXL_TextWidth(str[no])) / 2,
		AREA_LY + 100 + 2, str[no]);
    KXL_Font(NULL, 0xff, 0xff, 0xff);
    KXL_PutText(AREA_LX + (DRAW_WIDTH - KXL_TextWidth(str[no])) / 2,
		AREA_LY + 100, str[no]);
    KXL_Font( "-adobe-courier-bold-r-normal--14-*-*-*-*-*-iso8859-1", 0xff, 0xff, 0xff);
    break;
  }
}

