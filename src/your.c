#include "geki4.h"
#include "extern.h"

/****************************
  爆発
 ****************************/
RcHitEnum MoveBomb(CharacterData *my)
{
  if (++ my->Cnt1 < 10)
    return RcHitNone;
  return RcHitDel;
}

/****************************
  HPが無くなったら爆発する
 ****************************/
RcHitEnum HitEnemyToBomb(CharacterData *my, CharacterData *your)
{
  my->Hp --;
  if (my->Hp <= 0) {
    /*爆発*/
    KXL_PlaySound(SE_BOMB, KXL_SOUND_PLAY);
    Cchr.Attr     = AttrNone;
    Cchr.Target   = AttrNone;
    Cchr.X        = my->X;
    Cchr.Y        = my->Y;
    Cchr.Z        = my->Z;
    Cchr.Spr      = PixBomb;
    CopyYourNew(MoveBomb, HitNone, DrawPixmap3D);
    return RcHitBomb;
  } else
    KXL_PlaySound(SE_HIT, KXL_SOUND_PLAY);
  return RcHitNone;
}

/****************************
  "敵弾"移動
 ****************************/
RcHitEnum MoveEnemyShot(CharacterData *my)
{
  my->Z += my->AddZ;
  if (my->Z < 0)
    return RcHitDel;
  return RcHitNone;
}

/****************************
  "敵弾"出現
 ****************************/
void CreateEnemyShot(Sint16 x, Sint16 y, Sint16 z)
{
  KXL_PlaySound(SE_FIRE, KXL_SOUND_PLAY);
  Cchr.Attr     = AttrEShot;
  Cchr.Target   = AttrMy;
  Cchr.Spr      = PixEShot;
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.Z        = z;
  Cchr.AddZ     = -1;
  CopyYourNew(MoveEnemyShot, HitDelete, DrawPixmap3DNone);
}

/****************************
  跳ね返り移動
 ****************************/
RcHitEnum MoveBound(CharacterData *my)
{
  my->Z += my->AddZ;
  if (my->Z < 0)
    return RcHitDel;
  /** 移動範囲チェック **/
  /** 跳ね返り移動 **/
  CheckBound(my);
  my->X += my->AddX;
  my->Y += my->AddY;
  if (my->Spr == PixEnemy2 && Root->Stage > 0) {
    if (my->Cnt1 == 0) {
      if (rand() % (25 - Root->Stage * 5) == 0) {
	CreateEnemyShot(my->X, my->Y, my->Z);
      }
      my->Cnt1 = 20;
    }
  }
  if (my->Cnt1)
    my->Cnt1 --;
  return RcHitNone;
}

/****************************
  "顔"出現
 ****************************/
void CreateEnemy1(Sint16 x, Sint16 y)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Hp       = 1;
  Cchr.Score    = 10;
  Cchr.Spr      = PixEnemy1;
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.AddX     = ((rand()%3)-1)*2;
  Cchr.AddY     = ((rand()%3)-1)*2;
  Cchr.AddZ     = -1;
  Cchr.Z        = MAX_Z - 1;
  CopyYourNew(MoveBound, HitEnemyToBomb, DrawPixmap3D);
}

/****************************
  "飛行機"出現
 ****************************/
void CreateEnemy2(Sint16 x, Sint16 y)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Hp       = 1;
  Cchr.Score    = 30;
  Cchr.Spr      = PixEnemy2;
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.AddX     = ((rand() % 3) - 1) * (4 + rand() % 3);
  Cchr.AddY     = ((rand() % 3) - 1) * (4 + rand() % 3);
  Cchr.AddZ     = -1;
  Cchr.Z        = MAX_Z - 1;
  CopyYourNew(MoveBound, HitEnemyToBomb, DrawPixmap3D);
}

/****************************
  "顔"移動
 ****************************/
RcHitEnum MoveWall(CharacterData *my)
{
  my->Z += my->AddZ;
  if (my->Z < 0)
    return RcHitDel;
  return RcHitNone;
}

/****************************
  "顔"出現
 ****************************/
void CreateWall(Sint16 x)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy;
  Cchr.Spr      = PixWall;
  Cchr.X        = x;
  Cchr.Y        = Root->Stage == 0 ? 130 : 0;
  Cchr.AddZ     = -1;
  Cchr.Z        = MAX_Z - 1;
  CopyYourNew(MoveWall, HitNone, DrawPixmap3DNone);
}

