#include "geki4.h"
#include "extern.h"

/****************************
  "ショット"移動
 ****************************/
RcHitEnum MoveShot(CharacterData *my)
{
  my->Z += 2;
  if (my->Z > MAX_Z - 10)
    return RcHitDel;
  return RcHitNone;
}

/****************************
  "ショット"出現
 ****************************/
void CreateShot(Sint16 x, Sint16 y)
{
  Cchr.Attr     = AttrMShot;
  Cchr.Target   = AttrEnemy;
  Cchr.Spr      = PixShot;
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.Z        = -2;
  CopyMyNew(MoveShot, HitDelete, DrawPixmap3DNone);
}

/****************************
  自分移動
 ****************************/
RcHitEnum MoveMy(CharacterData *my)
{
  /** ポーズチェック **/
  if (Root->Key & KPause) {
    if (Root->MainFlag == MainGame) {
      Root->MainFlag = MainPause;
      return RcHitNone;
    }
  }
  /** 右に移動 **/
  if (Root->Key & KRight)
    my->X += my->AddX;
  /** 左に移動 **/
  if (Root->Key & KLeft)
    my->X -= my->AddX;
  /** 上に移動 **/
  if (Root->Key & KUp)
    my->Y -= my->AddY;
  /** 下に移動 **/
  if (Root->Key & KDown)
    my->Y += my->AddY;
  /** 移動範囲チェック **/
  if (my->X < DRAW_LEFT + my->Spr[0]->Image->Width / 2)
    my->X = DRAW_LEFT + my->Spr[0]->Image->Width / 2;
  else if (my->X > DRAW_RIGHT - my->Spr[0]->Image->Width / 2)
    my->X = DRAW_RIGHT - my->Spr[0]->Image->Width / 2;
  if (my->Y < DRAW_TOP + my->Spr[0]->Image->Height / 2)
    my->Y = DRAW_TOP + my->Spr[0]->Image->Height / 2;
  else if (my->Y > DRAW_BOTTOM - my->Spr[0]->Image->Height / 2)
    my->Y = DRAW_BOTTOM - my->Spr[0]->Image->Height / 2;
  /** 位置によりキャラ変更 **/
  my->FrameNo = (my->X < -150 ? 0 :
                 my->X < -50  ? 1 :
                 my->X > 150  ? 4 :
                 my->X > 50   ? 3 :
                 2);
  my->FrameNo = (my->Y < -50 ? my->FrameNo :
                 my->Y > 50  ? my->FrameNo + 10 :
                 my->FrameNo + 5);
  if (Root->Key & KShot) {
    if (my->Cnt2 == 0) {
      CreateShot(my->X, my->Y);
      my->Cnt2 = 8;
    }
  }
  if (my->Cnt2 > 0)
    my->Cnt2 --;
  return RcHitNone;
}

/****************************
  自分作成
 ****************************/
void ReCreateMy(void) {
  Uint8 i;

  for (i = 0; i < MAX_MY; i ++)
    Root->My[i]->Chr.Active = False;
  Root->My[0]->Chr.Attr    = AttrMy;
  Root->My[0]->Chr.Target  = AttrEnemy | AttrEShot;
  Root->My[0]->Chr.Spr     = PixMy;
  Root->My[0]->Move        = MoveMy;
  Root->My[0]->Hit         = HitMy;
  Root->My[0]->Draw        = DrawPixmapFrame3D;
  Root->My[0]->Chr.Active   = True;
  Root->My[0]->Chr.X        = 0;
  Root->My[0]->Chr.Y        = 0;
  Root->My[0]->Chr.Z        = 0;
  Root->My[0]->Chr.AddX     = 8;
  Root->My[0]->Chr.AddY     = 8;
  Root->My[0]->Chr.Cnt2     = 0;
  Root->MyNo = 1;
}

/****************************
  自分作成
 ****************************/
void CreateMy(void) {
  Uint8 i;

  for (i = 0; i < MAX_YOUR; i ++)
    Root->Your[i]->Chr.Active = False;
  Root->YourNo = 1;
  if (Root->MainFlag == MainGame) {
    Root->Score = 0;
    Root->Left = 2;
  }
  ReCreateMy();
  KXL_PlaySound(SE_BGM1, KXL_SOUND_PLAY_LOOP);
}

/****************************
  やられたー
 ****************************/
RcHitEnum HitMy(CharacterData *my, CharacterData *your)
{
  KXL_PlaySound(SE_DIE, KXL_SOUND_PLAY);
  Root->My[0]->Chr.Attr    = AttrNone;
  Root->My[0]->Chr.Target  = AttrNone;
  Root->My[0]->Chr.Spr  = PixBomb;
  Root->My[0]->Move     = MoveDie;
  Root->My[0]->Hit      = HitNone;
  Root->My[0]->Draw     = DrawPixmap3DNone;
  Root->My[0]->Chr.Cnt1 = 0;
  return RcHitNone;
}

/****************************
  死ぬ
 ****************************/
RcHitEnum MoveDie(CharacterData *my)
{
  Uint8 i;

  if (++ my->Cnt1 < 20)
    return RcHitNone;
  if (Root->Left == 0) {
    for (i = 0; i < MAX_MY; i ++)
      Root->My[i]->Chr.Active = False;
    Root->Cnt = -1;
    Root->MainFlag = MainGameOver;
  } else {
    Root->Left --;
    ReCreateMy();
  }
  return RcHitNone;
}

