#include "geki4.h"
#include "extern.h"

#define OP_BASE (AREA_LX+170)
#define CENTER(str) (AREA_LX + (DRAW_WIDTH - KXL_TextWidth((str))) / 2)
#define TO_MENU "push any key to menu"
#define TT "SpaceAryarya"
char *menu_str[]={
  "Game Start",
  "Key Operation",
  "Ranking",
  "Exit",
  " ",
  " ",
  " ",
  "move   - Cursor Up or Down",
  "select - z                ",
  ""
};
char *key_str[] = {
  "Key Operation",
  " ",
  "Up    - Cursor Up   ",
  "Down  - Cursor Down ",
  "Right - Cursor Right",
  "Left  - Cursor Left ",
  "Shot  - z           ",
  "Pause - s           ",
  ""
};
Uint8 menu = 0;
Uint8 sel = 0;

/**********************
  オープニング
 **********************/
Bool Opening(void)
{
  static Uint16 OldKey = KNone;
  Uint16 i, j;
  Uint8 buff[65];

  /* title */
  KXL_Rect rect = {AREA_LX, AREA_LY, DRAW_WIDTH, DRAW_HEIGHT};
  KXL_ClearFrame(rect);
  /** 背景 **/
  /*
  for (i = 0; i < 10; i ++)
    KXL_PutImage(PixBack[Root->Cnt % 3]->Image, AREA_LX + i * 50, AREA_RY - 120);
  */
  KXL_Font("-bitstream-charter-bold-*-normal-*-60-*-*-*-*-*-*-*", 0x00, 0x00, 0xff);
  KXL_PutText(CENTER(TT) + 2, AREA_LY + 50, TT);
  KXL_Font(NULL, 0x00, 0xff, 0xff);
  KXL_PutText(CENTER(TT), AREA_LY + 48, TT);
  KXL_Font("-adobe-courier-bold-r-normal--14-*-*-*-*-*-iso8859-1" , 0xff, 0xff, 0xff);
  if (menu == 0) { /* menu */
    for (i = 0; menu_str[i][0]; i ++) {
      if (i == sel)
        KXL_Font(NULL, 0xff, 0x00, 0x00);
      else
        KXL_Font(NULL, 0xff, 0xff, 0xff);
      KXL_PutText(CENTER(menu_str[i]), AREA_LY + 88 + i * 16, menu_str[i]);
    }
  } else if (menu == 1) { /* key operation */
    for (i = 0; key_str[i][0]; i ++) {
      KXL_Font(NULL, 0xff, 0xff, i == 0 ? 0x00 : 0xff);
      KXL_PutText(CENTER(key_str[i]), AREA_LY + 88 + i * 16, key_str[i]);
    }
    KXL_Font(NULL, 0x00, 0xff, 0xff);
    KXL_PutText(CENTER(TO_MENU), AREA_LY + 240, TO_MENU);
  } else if (menu == 2) { /* ranking */
    KXL_Font(NULL, 0xff, 0xff, 0x00);
    KXL_PutText(CENTER("Ranking Top 5"), AREA_LY + 88, "Ranking Top 5");
    KXL_Font(NULL, 0x00, 0xff, 0xff);
    sprintf(buff, "Top    Score     Stage   Name");
    KXL_PutText(CENTER(buff), AREA_LY + 108, buff);
    KXL_Font(NULL, 0xff, 0xff, 0xff);
    for (i = 0; i < 5; i ++) {
      sprintf(buff, "    %d    %08d     %d    %-8s", i + 1,
	      Ranking[i]->Score, Ranking[i]->Stage + 1,
	      Ranking[i]->Name);
      KXL_PutText(CENTER(buff), AREA_LY + 128 + i * 20, buff);
    }
    KXL_Font(NULL, 0x00, 0xff, 0xff);
    KXL_PutText(CENTER(TO_MENU), AREA_LY + 240, TO_MENU);
  }

  /* check key */
  if (OldKey != Root->Key) {
    if (OldKey == KNone && Root->Key) {
      if (menu == 0) {
        if (Root->Key & KUp) {
          if (sel == 0)
            sel = 3;
          else
            sel --;
        } else if (Root->Key & KDown) {
          if (sel == 3)
            sel = 0;
          else
            sel ++;
        }
        if (Root->Key & KShot){
          menu = sel;
          if (menu == 0) {
            Root->MainFlag = MainGame;
	    Root->Stage = 0;
	    LoadStage();
            CreateMy();
            Root->Cnt = -1;
          } else if (menu == 3)
            return True;
        }
      } else {
        menu = 0;
        Root->Key = KNone;
      }
    }
    OldKey = Root->Key;
  }
  return False;
}
