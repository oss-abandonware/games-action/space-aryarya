#ifndef _MISC_H_
#define _MISC_H_

void DrawPixmapFrame3D(CharacterData *my);
void DrawPixmap3D(CharacterData *my);
void DrawPixmap3DNone(CharacterData *my);
RcHitEnum HitDelete(CharacterData *my, CharacterData *your);
RcHitEnum HitNone(CharacterData *my, CharacterData *your);
RcHitEnum MoveNone(CharacterData *my);
void CheckBound(CharacterData *my);
void CheckStop(CharacterData *my);
void ClearAndGameOver(void);

#endif
