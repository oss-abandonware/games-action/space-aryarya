#include "geki4.h"
#include "extern.h"

Uint8 BossNo;
Uint8 Bosss;

/****************************
  爆発
 ****************************/
RcHitEnum MoveBossBomb(CharacterData *my)
{
  if (++ my->Cnt1 < 20)
    return RcHitNone;
  return RcHitBoss;
}

/****************************
  HPが無くなったら爆発する
 ****************************/
RcHitEnum HitBossToBomb(CharacterData *my, CharacterData *your)
{
  Uint8 i;

  my->Hp --;
  if (my->Hp <= 0) {
    /*爆発*/
    KXL_PlaySound(SE_BOMB, KXL_SOUND_PLAY);
    for (i = 0; i < Bosss; i ++) {
      Root->Your[BossNo + i]->Chr.Attr   = AttrNone;
      Root->Your[BossNo + i]->Chr.Target = AttrNone;
      Root->Your[BossNo + i]->Chr.Active = True;
      Root->Your[BossNo + i]->Chr.Spr    = PixBomb;
      Root->Your[BossNo + i]->Draw       = DrawPixmap3DNone;
      Root->Your[BossNo + i]->Hit        = HitNone;
      Root->Your[BossNo + i]->Move       = i == 0 ? MoveBossBomb : MoveBomb;
      Root->Your[BossNo + i]->Chr.Cnt1   = 0;
    }
  } else
    KXL_PlaySound(SE_HIT, KXL_SOUND_PLAY);
  return RcHitNone;
}

/****************************
  ボス1設定
 ****************************/
void CreateBoss1(void)
{
  Uint8 i;

  Bosss  = 15;
  BossNo = MAX_YOUR - Bosss - 1;
  for (i = 0; i < Bosss; i ++) {
    Root->Your[BossNo + i]->Chr.Attr   = AttrEnemy;
    Root->Your[BossNo + i]->Chr.Target = AttrMShot;
    Root->Your[BossNo + i]->Chr.Active = True;
    Root->Your[BossNo + i]->Chr.Spr    = PixBoss3;
    Root->Your[BossNo + i]->Chr.X      = 0;
    Root->Your[BossNo + i]->Chr.Y      = 0;
    Root->Your[BossNo + i]->Chr.Z      = MAX_Z - 1;
    Root->Your[BossNo + i]->Draw       = DrawPixmap3D;
    Root->Your[BossNo + i]->Hit        = HitNone;
    Root->Your[BossNo + i]->Move       = MoveNone;
    Root->Your[BossNo + i]->Chr.Cnt1   = 0;
    Root->Your[BossNo + i]->Chr.Cnt2   = 0;
    Root->Your[BossNo + i]->Chr.Cnt3   = 0;
    Root->YourNo ++;
  }
  Root->Your[BossNo]->Chr.Spr      = PixBoss1;
  Root->Your[BossNo]->Hit          = HitBossToBomb;
  Root->Your[BossNo]->Move         = MoveBoss2;
  Root->Your[BossNo]->Chr.Hp       = 20;
  Root->Your[BossNo]->Chr.Score    = 1000;
  Root->Your[BossNo]->Chr.AddX     = 4;
  Root->Your[BossNo]->Chr.AddY     = 4;
  Root->Your[BossNo]->Chr.AddZ   = -1;
}

/****************************
  ステージ1
 ****************************/
RcHitEnum MoveBoss1(CharacterData *my)
{
  Uint8 i;

  /** 胴体移動 **/
  my->Cnt1 ++;
  if (my->Cnt1 % 2) {
    for (i = MAX_YOUR - 1; i > BossNo; i --) {
      Root->Your[i]->Chr.X = Root->Your[i - 1]->Chr.X;
      Root->Your[i]->Chr.Y = Root->Your[i - 1]->Chr.Y;
      Root->Your[i]->Chr.Z = Root->Your[i - 1]->Chr.Z;
    }
  }
  /** 頭移動 **/
  if (my->Z + my->AddZ < 0 ||
      my->Z + my->AddZ >= MAX_Z)
    my->AddZ = -(my->AddZ);
  CheckBound(my);
  my->X += my->AddX;
  my->Y += my->AddY;
  my->Z += my->AddZ;
  my->Spr = my->AddZ < 0 ? PixBoss1 : PixBoss2;
  if (my->Cnt1 > 20) {
    if (my->Z > 10 && my->AddZ < 0 && rand() % 2)
      CreateEnemyShot(my->X, my->Y, my->Z);
    my->Cnt1 = 0;
  } else
    my->Cnt1 ++;
 
  return RcHitNone;
}

/****************************
  ステージ2
 ****************************/
RcHitEnum MoveBoss2(CharacterData *my)
{
  Uint8 i;

  /** 胴体移動 **/
  my->Cnt2 ++;
  if (my->Cnt2 % 2) {
    for (i = MAX_YOUR - 1; i > BossNo; i --) {
      Root->Your[i]->Chr.X = Root->Your[i - 1]->Chr.X;
      Root->Your[i]->Chr.Y = Root->Your[i - 1]->Chr.Y;
      Root->Your[i]->Chr.Z = Root->Your[i - 1]->Chr.Z;
    }
  }
  my->X += my->AddX;
  my->Y += my->AddY;
  switch (my->Cnt1) {
  case 0: /**手前に移動 */
    my->Z --;
    if (my->Z == 0) {
      my->AddY  = my->Y < 0 ? 8 : -8;
      my->Cnt1 = 1;
    } else
      CheckBound(my);
    break;
  case 1: /**上下へ**/
    if (my->Y > DRAW_BOTTOM - my->Spr[0]->Image->Height / 2 ||
	my->Y < DRAW_TOP + my->Spr[0]->Image->Height / 2) {
      my->AddX = my->X < 0 ? 8 : -8;
      my->Cnt1 = 2;
    }
    break;
  case 2: /**左右右へ**/
    if (my->X > DRAW_RIGHT - my->Spr[0]->Image->Width / 2 ||
	my->X < DRAW_LEFT + my->Spr[0]->Image->Width / 2)
      my->Cnt1 = 3;
    break;
  case 3: /**奥へ**/
    my->Z ++;
    if (my->Z == MAX_Z - 1) {
      my->AddX = ((rand() % 3) - 1) * 8;
      my->AddY = ((rand() % 3) - 1) * 8;
      my->Cnt1 = 0;
    }
    break;
  }
  CheckStop(my);
  return RcHitNone;
}
