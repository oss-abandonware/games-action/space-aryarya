/*
**  Geki4
**  Copyright (C) 2001, kacchan
**  E-M@il address   fc3srx@mwnet.or.jp
**  Homepage address http://www2.mwnet.or.jp/~fc3srx7
*/
#ifndef _GEKI4_H_
#define _GEKI4_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <KXL.h>

/** フレームレート **/
#define FRAME_RATE 40

/** キー **/
#define KeyUp    KXL_KEY_Up
#define KeyDown  KXL_KEY_Down
#define KeyRight KXL_KEY_Right
#define KeyLeft  KXL_KEY_Left
#define KeyShot  KXL_KEY_z
#define KeyPause KXL_KEY_s

/** キーマスク **/
#define KNone      0
#define KUp        (1L << 0)
#define KDown      (1L << 1)
#define KRight     (1L << 2)
#define KLeft      (1L << 3)
#define KShot      (1L << 4)
#define KPause     (1L << 5)

#define KUpMask    ~KUp
#define KDownMask  ~KDown
#define KRightMask ~KRight
#define KLeftMask  ~KLeft
#define KShotMask  ~KShot
#define KPauseMask ~KPause

/** キャラクタの識別子**/
#define AttrNone     0
#define AttrMy       (1L << 0)
#define AttrMShot    (1L << 1)
#define AttrEnemy    (1L << 2)
#define AttrEShot    (1L << 3)

/** ウィンドウサイズ **/
#define DRAW_WIDTH   500
#define DRAW_HEIGHT  300
#define FREE_AREA    60
#define PICT_WIDTH   (DRAW_WIDTH + FREE_AREA * 2)
#define PICT_HEIGHT  (DRAW_HEIGHT + FREE_AREA * 2)
#define AREA_LX      FREE_AREA
#define AREA_LY      FREE_AREA
#define AREA_RX      (PICT_WIDTH - FREE_AREA)
#define AREA_RY      (PICT_HEIGHT - FREE_AREA)
#define DRAW_LEFT    -DRAW_WIDTH / 2
#define DRAW_RIGHT   DRAW_WIDTH / 2
#define DRAW_TOP     -DRAW_HEIGHT / 2
#define DRAW_BOTTOM  DRAW_HEIGHT / 2
#define ADD_X        AREA_LX + DRAW_RIGHT
#define ADD_Y        AREA_LY + DRAW_BOTTOM
#define CHECK_PER    0.05
#define CHECK_MAX    4
#define BASE_Z       15
#define MAX_Z        60
#define STAGE_MAX    2

/** 各種最大値 **/
#define MAX_YOUR    80 /** 敵&敵弾 **/
#define MAX_MY      16  /** プレイヤー&プレイヤー弾 **/

/** サウンド **/
#define SE_BGM1     0
#define SE_BOMB     1
#define SE_HIT      2
#define SE_FIRE     3
#define SE_DIE      4
#define SE_BOSS     5

/** メイン動作 **/
typedef enum {
  MainOpening,
  MainGame,
  MainClear,
  MainGameOver,
  MainEnding,
  MainPause
} MainEnum;

/*移動後の戻り値*/
typedef enum {
  RcHitNone,
  RcHitDel,
  RcHitBomb,
  RcHitBoss
} RcHitEnum;

/** ピックス情報 **/
typedef struct {
  KXL_Image *Image;
  Sint16 x[4], y[4];  /** 0:left&top,   1:right&top,
                         2:left&bottom,3:right&bottom **/
  Uint16 AddX, AddY; /** 中心座標からの加算値 **/   
} PixData;

/** キャラクタデータ **/
typedef struct {
  Bool    Active;           /** 生死 **/
  Uint16  Attr;             /** 識別子 **/
  Uint16  Target;           /** ターゲット **/
  Sint16  Hp;               /** 耐久力 **/
  Sint16  Cnt1, Cnt2, Cnt3; /**  **/
  Uint32  Score;            /** 点数 **/
  Sint16  X, Y, Z;          /** スプライト座標 **/
  Sint16  AddX, AddY, AddZ; /** スプライト移動量加算値 **/
  Uint8   FrameNo;          /** フレームNo. **/
  PixData **Spr;            /** イメージ **/
} CharacterData;

/** 汎用キャラクタデータ **/
typedef struct {
  CharacterData Chr;
  void (*Draw)(CharacterData *my);
  RcHitEnum (*Move)(CharacterData *my);
  RcHitEnum (*Hit)(CharacterData *my, CharacterData *your);
} CharacterObject;

/** 管理用 **/
typedef struct {
  MainEnum MainFlag;      /** メインフラグ **/
  Uint16   Key;           /** キーコード **/
  Uint8    MyNo;          /** 自分側のキャラクタ数 **/
  Uint8    YourNo;        /** 敵側のキャラクタ数 **/
  Sint16   Cnt;           /** 汎用カウンタ **/
  Bool     WaitFlag;      /** ポーズフラグ **/
  CharacterObject **My;   /** 自分側のキャラクタ **/
  CharacterObject **Your; /** 敵側のキャラクタ **/
  Uint32   Score;         /** スコア **/
  Uint32   HiScore;       /** ハイスコア **/
  Uint8    Left;          /** 残機 **/
  Uint8    Stage;         /** ステージ **/
  Uint8    StageMax;      /** 敵出現パターン数 **/
  Uint16   EnemyCnt;      /** 敵出現カウンタ **/
} RootData;

/*ランキング*/
typedef struct {
  Uint32 Score;
  Uint8  Stage;
  Uint8  Name[16];
} RankingData;

/*ステージデータ*/
typedef struct {
  Uint16 Time;         /* 敵出現時間 */
  Uint8  CreateNo;     /* 敵No. */
  Uint8  Max;          /* 敵出現回数 */
  Uint16 Step;         /* 敵出現間隔 */
  Uint16 StepTime;
  Bool Flag;
} StageData;

#endif
